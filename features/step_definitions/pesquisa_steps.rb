Dado('que pesquiso por {string}') do |string|
  @home.input_search.send_keys string
end

Quando('clico na primeira opção listada') do
  @home.selecionar_produto
end

Então('imprimo na tela o título e valor do primeiro resultado') do
  scroll_element(@home.card_prod[0])
  sleep 2
  @home.modal_closed.visible? 
  @home.modal_closed.click
  @home.imprimir_nome_produto
end

Quando('seleciono a segunda pagina de resultados') do
  @home.paginacao[0].click
end

Então('valido a segunda pagina') do
  @home.card_prod_01[0].visible?
end