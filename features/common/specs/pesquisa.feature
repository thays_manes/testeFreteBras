#utf-8 
# language: pt
@olx

Funcionalidade: Pesquisa de um produto dentro do site OLX

 @CT01 
    Cenário: Realizar uma pesquisa no site da OLX e imprimir SOMENTE título e valor do primeiro resultado
    Dado que pesquiso por 'Secador de cabelo'
    Quando clico na primeira opção listada
    Então imprimo na tela o título e valor do primeiro resultado

 @CT02
    Cenário: Realizar uma pesquisa no site da OLX que contenha paginação, tirar um print do anúncio da segunda página 
    Dado que pesquiso por 'Secador de cabelo'
    Quando clico na primeira opção listada
    E seleciono a segunda pagina de resultados
    Então valido a segunda pagina
