# MAPEAMENTO DA PAGINA PRINCIPAL DO SITE
class HomeScreen < SitePrism::Page
  include Helpers
  include BaseScreen
  include HomeModule

  element :input_search, '#searchtext'
  element :modal_closed, '.zi8mbi-7.gVLOzw'
  elements :card_prod, '.Image__StyledImg-b6zkca-0.iWRsZI'
  elements :paginacao, '.sc-hmzhuo.sc-1koxwg0-0.eJGOcN.sc-jTzLTM.iwtnNi'
  elements :card_prod_01, '.sc-101cdir-1.fwpxEI'
end
