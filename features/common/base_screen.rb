# frozen_string_literal: true

module BaseScreen
  # metodo usado para fazer o scroll da tela caso o elemento nao esteja na mesma, ate encontralo
  def scroll_element(element)
    execute_script('arguments[0].scrollIntoView(true);', element)
  end
end
